<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 13.03.2016
 * Time: 7:14
 */
$centralfr = array(
    "Москва", "Воронеж", "Старый Оскол"
);
$southfr = array(
    "Ростов на-Дону", "Волгоград", "Краснодар"
);
$nwestfr = array(
    "Великий Устюг", "Сыктывкар", "Санкт Петербург"
);
$deastfr = array(
    "Хабаровск", "Комсомольск на-Амуре", "Петропавловск Камчатский"
);
$siberiafr = array(
    "Улан Удэ", "Омск", "Новосибирск"
);
$uralfr = array(
    "Екатеринбург", "Ханты Мансийск", "Новый Уренгой"
);
$privolzhfr = array(
    "Нижний Новгород", "Казань", "Йошкар Ола"
);
$nkavkazfr = array(
    "Мин Воды", "Махачкала", "Нальчик"
);
$krmfr = array(
    "Севастопль", "Керчь", "Ялта"
);

$allcity_array = array_merge($centralfr, $southfr, $nwestfr, $deastfr, $siberiafr, $uralfr, $privolzhfr, $nkavkazfr, $krmfr);

$city2words = array();
$word1st = array();
$word2nd = array();
    foreach ($allcity_array as $v) {
        $city2words[] = $v;
    };
    foreach ($city2words as $k => $v) {
        $separated = explode(" ", $v);
        $result = count($separated);
        unset($city2words[$k]);
        if ($result > 1) {
            array_push($word1st, $separated[0]);
            array_push($word2nd, $separated[1]);
        }
    }
$secondwords = array();
shuffle($word2nd);
$secondwords = $word2nd;
$new_centralfr = array();
$new_centralfr = $centralfr;
$new_southfr = array();
$new_southfr = $southfr;
$new_nwestfr = array();
$new_nwestfr = $nwestfr;
$new_deastfr = array();
$new_deastfr = $deastfr;
$new_siberiafr = array();
$new_siberiafr = $siberiafr;
$new_uralfr = array();
$new_uralfr = $uralfr;
$new_privolzhfr = array();
$new_privolzhfr = $privolzhfr;
$new_nkavkazfr = array();
$new_nkavkazfr = $nkavkazfr;
$new_krmfr = array();
$new_krmfr = $krmfr;
$new_city = array();
    for ($l = 0; $l < count($word1st); $l++) {
        $new_city[$l] = $word1st[$l] . " " . $secondwords[$l];
    }
print_r($new_city); //тут простой вывод новых городов одним массивом
foreach ($new_centralfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_centralfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_southfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_southfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_nwestfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_nwestfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_deastfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_deastfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_siberiafr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_siberiafr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_uralfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_uralfr [$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_privolzhfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_privolzhfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_nkavkazfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_nkavkazfr[$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
foreach ($new_krmfr as $k => $v){
    $sep = explode(" ", $v);
    $result = count($sep);
    if ($result > 1){
        $new_krmfr [$k] = $sep[0]." ".$secondwords[0];
        unset($secondwords[0]);
        $secondwords = array_values($secondwords);
    }
}
$array = array(
    "Центральный федеральный округ" => $centralfr,
    "Южный федеральный округ" => $southfr,
    "Северо-Западный федеральный округ" => $nwestfr,
    "Дальневосточный федеральный округ" => $deastfr,
    "Сибирский федеральный округ" => $siberiafr,
    "Уральский федеральный округ" => $uralfr,
    "Приволжский федеральный округ" => $privolzhfr,
    "Северо-Кавказский федеральный округ" => $nkavkazfr,
    "Крымский федеральный округ" => $krmfr,
);

$new_array = array(
    "Центральный федеральный округ" => $new_centralfr,
    "Южный федеральный округ" => $new_southfr,
    "Северо-Западный федеральный округ" => $new_nwestfr,
    "Дальневосточный федеральный округ" => $new_deastfr,
    "Сибирский федеральный округ" => $new_siberiafr,
    "Уральский федеральный округ" => $new_uralfr,
    "Приволжский федеральный округ" => $new_privolzhfr,
    "Северо-Кавказский федеральный округ" => $new_nkavkazfr,
    "Крымский федеральный округ" => $new_krmfr,
);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>3rd lesson</title>
</head>
<body>
<table border="1" cellspacing="0" cellpadding="10">
    <caption><h1>Старый мир</h1></caption>
        <?php foreach($array as $k => $v){
            echo "<tr>
                  <td><strong>{$k}:</strong></td>
                  <td>$v[0]</td>
                  <td>$v[1]</td>
                  <td>$v[2]</td>
                  </tr>";
        }
        ?>
</table>
<table border="1" cellspacing="0" cellpadding="10">
    <caption><h1>Новый мир</h1></caption>
    <?php foreach($new_array as $k => $v){
        echo "<tr>
                  <td><strong>{$k}:</strong></td>
                  <td>$v[0]</td>
                  <td>$v[1]</td>
                  <td>$v[2]</td>
                  </tr>";
    }
    ?>
</table>
</body>
</html>